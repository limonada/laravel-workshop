<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $dates = [""];
    protected $fillable = [
    	"classroom_id", "name", "uuid"
    ];

    public function classroom() {
    	return $this->belongsTo("\App\Classroom");
    }
}
