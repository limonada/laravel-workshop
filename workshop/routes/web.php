<?php
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home/{user_id}', "HomeController@index");
Route::get('/student/{student_id}', "HomeController@list");
